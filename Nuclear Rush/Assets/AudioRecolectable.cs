using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioRecolectable : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip sonidoRecolectable;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void ReproducirAudio()
    {
        audioSource.PlayOneShot(sonidoRecolectable);
    }
}
