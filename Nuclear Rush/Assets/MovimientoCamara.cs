using NUnit.Framework.Constraints;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MovimientoCamara : MonoBehaviour
{
    bool moviendo;
    public int probabilidad;
    public string posicion;
    public Vector3 posicionInicial, rotacionInicial;
    public Vector3 posicionDerecha,rotacionDerecha,posicionIzquierda,rotacionIzquierda;
    string ultimaPosicion;

    Vector3 posicionDestino, rotacionDestino;

    public float tolerancia, velocidadDeMovimiento, tiempoAIntentar;


    private void Start()
    {
        Invoke("DesicionAleatoria", 3f);
    }

    void FixedUpdate()
    {
        if (GameObject.Find("LevelManager").GetComponent<LevelManager>().Vivo)
        {
            ActualizarDestino();
            if (moviendo)
            {
                MoverCamara();
            }
        }
        
    }

    public void MoverCamara()
    {
        
        //MOVIMIENTO-------------------------------------- -
        if (transform.position.x != posicionDestino.x)
        {
            if (transform.position.x - posicionDestino.x > tolerancia)
            {
                transform.Translate(Vector3.left * velocidadDeMovimiento * Time.deltaTime);
            }
            else if (transform.position.x - posicionDestino.x < -tolerancia)
            {
                transform.Translate(Vector3.right * velocidadDeMovimiento * Time.deltaTime);
            }
        }
        

        if (transform.position.y != posicionDestino.y)
        {
            if (transform.position.y - posicionDestino.y > tolerancia)
            {
                transform.Translate(Vector3.down * velocidadDeMovimiento * Time.deltaTime);
            }
            else if (transform.position.y - posicionDestino.y < -tolerancia)
            {
                transform.Translate(Vector3.up * velocidadDeMovimiento * Time.deltaTime);
            }
        }

        if (transform.position.z != posicionDestino.z)
        {
            if (transform.position.z - posicionDestino.z > tolerancia)
            {
                transform.Translate(Vector3.back * velocidadDeMovimiento * Time.deltaTime);
            }
            else if (transform.position.z - posicionDestino.z < -tolerancia)
            {
                transform.Translate(Vector3.forward * velocidadDeMovimiento * Time.deltaTime);
            }
        }



        //ROTACION----------------------------------

        Quaternion rotacion = Quaternion.Euler(0,rotacionDestino.y,0);
        
        transform.rotation = Quaternion.Lerp(transform.rotation, rotacion, 0.008f);




    }

    public void DesicionAleatoria()
    {
        ultimaPosicion = posicion;
        int aleatorio = Random.Range(0, 100);
        if(aleatorio < probabilidad)
        {
            int aleatorio2 = Random.Range(0, 3);
            if(aleatorio2 == 0)
            {
                posicion = "Centro";
            }
            else if(aleatorio2 == 1)
            {
                posicion = "Derecha";
            }
            else
            {
                if(ultimaPosicion == "Derecha")
                {
                    posicion = "Centro";
                    Debug.Log("Se fue a centro pero tocaba izqueirda");
                }
                else
                {
                    posicion = "Izquierda";
                }
            }
            
            moviendo = true;
        }
        if (GameObject.Find("LevelManager").GetComponent<LevelManager>().Vivo)
        {
            Invoke("DesicionAleatoria", tiempoAIntentar);
        }

    }

    public void ActualizarDestino()
    {
        
        if(posicion == "Centro")
        {
            posicionDestino = posicionInicial;
            rotacionDestino = rotacionInicial;
        }
        else if(posicion == "Derecha")
        {
            posicionDestino= posicionDerecha;
            rotacionDestino = rotacionDerecha;
        }
        else if (posicion == "Izquierda")
        {
            posicionDestino = posicionIzquierda;
            rotacionDestino = rotacionIzquierda;
        }
    }
}
