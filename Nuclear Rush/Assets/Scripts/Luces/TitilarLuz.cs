using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TitilarLuz : MonoBehaviour
{
    public GameObject luz;
    public int probabilidadDeParpadeo;
    float coolDown;
    int random;

    private void Start()
    {
        Titilar();
    }

    public void Titilar()
    {
        coolDown = Random.Range(0.3f,1);
        Invoke("Titilar", coolDown);
        Parpadear();
        
    }

    public void Parpadear()
    {
        random = Random.Range(0, 500);
        

        for (int i = 0; i < random; i++)
        {
            int aleatorio = Random.Range(0, 100);
            if(aleatorio < probabilidadDeParpadeo)
            {
                if (luz.activeSelf)
                {
                    luz.SetActive(false);
                }
                else
                {
                    luz.SetActive(true);
                }
            }
        }
    }

}
