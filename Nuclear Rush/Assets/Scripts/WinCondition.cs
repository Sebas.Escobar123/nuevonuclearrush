using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour
{
    PlayerController controller;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            controller = GameObject.Find("Jugador").GetComponent<PlayerController>();

            controller.posicionInicial.x = 50;
            controller.velocidadDeRecuperacion = 3.5f;
            GameObject.Find("LevelManager").GetComponent<LevelManager>().victoria = true;
        }
        
    }
}
