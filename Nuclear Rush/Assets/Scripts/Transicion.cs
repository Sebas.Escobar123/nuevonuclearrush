using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transicion : MonoBehaviour
{
    Transformacion transformacion;
    ContadorDeResiduo contador;
    LevelManager manager;
    RazonDeMuerte razon;

    bool transformado;

    private void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        razon = GameObject.Find("LevelManager").GetComponent<RazonDeMuerte>();
        transformacion = GameObject.Find("LevelManager").GetComponent<Transformacion>();
        contador = GameObject.Find("ContadorDeResiduos").GetComponent<ContadorDeResiduo>();
        transformado = false;

    }




    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Transicion"))
        {
            if (transformacion.cantidadParaTransformarse[manager.ObtenerTrasnformacionActual()] <= contador.residuo)
            {
                transformacion.Transformar();
                transformado = true;
                Invoke("Cooldown", 2);
                contador.residuo = 0;
                contador.SubirResiduo(0);
            }
            else if(!transformacion.acabaDeTransicionar)
            {
                Debug.Log("Entr� a que no le dio el residuo");
                razon.ActualizarRazonDePerdida("Sustancia toxica insuficiente :(");
                manager.EstadoVivo(false);
            }

        }
    }

    public void Cooldown()
    {
        transformado = false;
    }
}
