using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    
    LevelManager manager;
    public float tiempoRestante;
    public TextMeshProUGUI textoTiempo;
    LuzExplosion luz;
    public GameObject luzExplosion;
    RazonDeMuerte razon;
    void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        razon = GameObject.Find("LevelManager").GetComponent<RazonDeMuerte>();
        luz = luzExplosion.GetComponent<LuzExplosion>();
        tiempoRestante = manager.tiempoDeJuego;
    }

    private void FixedUpdate()
    {

        if(tiempoRestante >0)
        {
            textoTiempo.text = Mathf.Round(tiempoRestante).ToString();
        }
        else
        {
            textoTiempo.text = Mathf.Round(0).ToString();
            luz.exploto = true;
        }
        tiempoRestante -= Time.deltaTime;
        if (!manager.victoria)
        {
            


            if (tiempoRestante < 0)
            {
                manager.EstadoVivo(false);
                razon.ActualizarRazonDePerdida("�POOOOOM!");
                luz.exploto = true;
            }
            if (tiempoRestante < 11)
            {
                textoTiempo.color = Color.red;
                if (GameObject.Find("Time"))
                {
                    Destroy(GameObject.Find("Time"));
                }
                if (textoTiempo.fontSize < 300)
                {
                    textoTiempo.fontSize += 0.75f;
                }
            }
        }
        
    }
}
