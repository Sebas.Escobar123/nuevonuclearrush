using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MostrarPuntaje : MonoBehaviour
{
    LevelManager manager;
    public TextMeshProUGUI puntuacion;
    void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
    }

    // Update is called once per frame
    void Update()
    {
        puntuacion.text = manager.puntuacion.ToString();
    }
}
