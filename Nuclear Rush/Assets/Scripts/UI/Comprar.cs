using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;



public class Comprar : MonoBehaviour
{

    public int precio;

   

    public TextMeshProUGUI precioText;
    public TextMeshProUGUI TiempoText;
    public TextMeshProUGUI VelocidadText;
    DatosDeTienda datos;

    public LevelManager manager;

    //colores
    private Color colorBlanco = Color.white;
    private Color colorRojo = Color.red;


    public AudioClip sonido; // Variable para el sonido
    private AudioSource audioSource; // Componente AudioSource





    private void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        datos = GameObject.Find("LevelManager").GetComponent<DatosDeTienda>();
        precioText.text = precio.ToString();
        if(TiempoText != null)
        {

            TiempoText.text = "Tiempo: " + manager.tiempoDeJuego.ToString() + " s";
        }
        if(VelocidadText != null)
        {
            VelocidadText.text = "Velocidad: " + manager.velocidadInicial.ToString();
        }


        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = sonido;
    }

    private void Update()
    {
        
        precioText.text = precio.ToString();
        if(TiempoText != null && datos.mejorasDeTiempoCompradas<9)
        {
            if (datos.mejorasDeTiempoCompradas == 0)
            {
                precio = 150;
            }
            else
            {
                precio = 150 * datos.mejorasDeTiempoCompradas;
            }
            TiempoText.text = "Tiempo: " + manager.tiempoDeJuego + " s";
        }

        if(VelocidadText != null && datos.mejorasDeVelocidadCompradas <9)
        {
            if (datos.mejorasDeVelocidadCompradas == 0)
            {
                precio = 100;
            }
            else
            {
                precio = 100 * datos.mejorasDeVelocidadCompradas;
            }
            
            VelocidadText.text = "Velocidad: " + (manager.velocidadInicial - 1.5f);
        }


        // Cambiar el color del texto de precio basado en la puntuaci�n
        if (manager.puntuacion < precio)
        {
            precioText.color = colorRojo;
        }
        else
        {
            precioText.color = colorBlanco;
        }

    }
    public void HacerCompra(string item)
    {

        
        if(manager.puntuacion >= precio)
        {
            

            if(item == "Tiempo" && datos.mejorasDeTiempoCompradas<8)
            {
                manager.puntuacion -= precio;
                manager.tiempoDeJuego += 15;
                datos.mejorasDeTiempoCompradas++;

                Debug.Log("Compr� tiempo");
            }
            else if(item == "Velocidad" && datos.mejorasDeVelocidadCompradas <8)
            {
                manager.puntuacion -= precio;
                Debug.Log("Compr� velocidad");
                manager.velocidadInicial += 0.2f;
                datos.mejorasDeVelocidadCompradas++;
            }
        }
    }

    public void ReproducirSonido()
    {
        audioSource.Play();
    }
}
