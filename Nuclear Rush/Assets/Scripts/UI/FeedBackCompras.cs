using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedBackCompras : MonoBehaviour
{
    [Header("Sirve: 'Tiempo', 'Velocidad'")]
    public string mejora;
    public GameObject[] rellenos;
    public AudioClip sonido; // Variable para el sonido
    private AudioSource audioSource; // Componente AudioSource

    DatosDeTienda datos;
    int constante, contador;

    private void Start()
    {
        datos = GameObject.Find("LevelManager").GetComponent<DatosDeTienda>();

        // A�adir el componente AudioSource y asignar el AudioClip
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = sonido;
    }

    private void Update()
    {
        RevisarContador();

        contador = constante;

        foreach (var relleno in rellenos)
        {
            if (contador != 0)
            {
                contador--;
                relleno.SetActive(true);
            }
            else
            {
                relleno.SetActive(false);
            }
        }
    }

    public void RevisarContador()
    {
        if (mejora == "Tiempo")
        {
            constante = datos.mejorasDeTiempoCompradas;
        }
        else if (mejora == "Velocidad")
        {
            constante = datos.mejorasDeVelocidadCompradas;
        }
        else
        {
            print("No se encontr� mejora de: " + mejora);
        }
    }

    // M�todo para ser llamado cuando se presione el bot�n
    public void ReproducirSonido()
    {
        audioSource.Play();
    }
}