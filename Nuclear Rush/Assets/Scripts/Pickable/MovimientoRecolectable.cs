using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoRecolectable : MonoBehaviour
{

    public float velocidad;
    public float distancia;
    private void Start()
    {
        Invoke("Invertir", distancia);
    }


    private void Update()
    {
        transform.position = new Vector3(transform.position.x,
                                        transform.position.y + (velocidad * Time.deltaTime),
                                        transform.position.z);

    }

    void Invertir()
    {
        velocidad = -velocidad;
        Invoke("Invertir", distancia);
    }
}
