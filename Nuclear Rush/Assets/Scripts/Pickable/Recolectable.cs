using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recolectable : MonoBehaviour
{
    ContadorDeResiduo contador;
    LevelManager manager;
    public int cantidad,puntosQueOtorga;
    bool cogido;

    public AudioSource Pick;

    

    private void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        contador = GameObject.Find("ContadorDeResiduos").GetComponent<ContadorDeResiduo>();
        if(contador == null)
        {
            Debug.Log("ContadorNoEncontrado");
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!cogido)
        {
            cogido = true;
            contador.SubirResiduo(cantidad);
            manager.ActualizarPuntuacionActual(puntosQueOtorga);
            GameObject.Find("AudioRecolectable").GetComponent<AudioRecolectable>().ReproducirAudio();
            Destroy(gameObject);
            


        }
        
        
    }

}
