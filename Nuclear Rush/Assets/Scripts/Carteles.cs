using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carteles : MonoBehaviour
{
    
    public GameObject[] prohibidos, permitidos;

    LevelManager manager;

    void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        foreach (var p in prohibidos)
        {
            p.SetActive(false);
        }
        foreach (var p in permitidos)
        {
            p.SetActive(false);
        }

        prohibidos[manager.ObtenerTrasnformacionActual()].SetActive(true);
        if(manager.ObtenerTrasnformacionActual() == 2)
        {
            permitidos[0].SetActive(true);
        }
        else
        {
            permitidos[manager.ObtenerTrasnformacionActual() + 1].SetActive(true);
        }
        

    }


}
