using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FueraDeMapa : MonoBehaviour
{
    LevelManager manager;
    RazonDeMuerte razon;
    void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        razon = GameObject.Find("LevelManager").GetComponent<RazonDeMuerte>();
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("GameOver"))
        {
            Debug.Log("TOCO EL TRIGGER GAMEOVER");
            manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
            manager.EstadoVivo(false);
            razon.ActualizarRazonDePerdida("Te quedaste demasiado atras");
            
        }
    }
}
