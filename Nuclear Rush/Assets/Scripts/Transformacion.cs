using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Transformacion : MonoBehaviour
{
    LevelManager manager;
    public int[] cantidadParaTransformarse;

    public bool acabaDeTransicionar;

    public GameObject[] transformaciones = new GameObject[3];
    private void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        acabaDeTransicionar = false;
        
    }
    public void Transformar()
    {
        if (!acabaDeTransicionar)
        {
            acabaDeTransicionar = true;
            Invoke("CooldownTransicion", 2f);
            if (manager.ObtenerTrasnformacionActual() == 0)
            {
                transformaciones[0].SetActive(false);
                transformaciones[1].SetActive(true);
                manager.transformacionActual++;
                Debug.Log("---Transformación Existosa---");
                transformaciones[1].GetComponent<Acido>().Referenciar();
            }
            else if (manager.ObtenerTrasnformacionActual() == 1)
            {
                transformaciones[1].SetActive(false);
                transformaciones[2].SetActive(true);
                manager.transformacionActual++;
                Debug.Log("---Transformación Existosa---");
            }
            else if (manager.ObtenerTrasnformacionActual() == 2)
            {
                transformaciones[2].SetActive(false);
                transformaciones[0].SetActive(true);
                manager.transformacionActual = 0;
                Debug.Log("---Transformación Existosa---");
            }
        }
        
        
    }

    public void RellenarMatriz()
    {
        transformaciones[0] = GameObject.Find("Jugador");
        transformaciones[1] = GameObject.Find("MonoAraña");
        transformaciones[2] = GameObject.Find("Pajaro");

        Debug.Log("Matriz rellenada: 1. " + transformaciones[0] + " /// 2. " + transformaciones[1] + " /// 3. " + transformaciones[2]);

        transformaciones[1].SetActive(false);
        transformaciones[2].SetActive(false);

    }

    public void CooldownTransicion()
    {
        acabaDeTransicionar = false;
        
    }
}
