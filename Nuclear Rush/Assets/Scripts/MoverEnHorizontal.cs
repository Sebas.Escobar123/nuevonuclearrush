using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverEnHorizontal : MonoBehaviour
{
    public Vector3 posicionDeseada;
    public float velocidadDeMovimiento;
    public float delayParaEmpezarMovimiento;
    public float tolerancia;
    

    // Update is called once per frame
    void Update()
    {
        if(delayParaEmpezarMovimiento > 0)
        {
            delayParaEmpezarMovimiento -= Time.deltaTime;
        }
        else
        {
            Mover();
        }
    }

    void Mover()
    {

        if(transform.localPosition.x != posicionDeseada.x)
        {
            if(transform.localPosition.x - posicionDeseada.x > tolerancia)
            {
                transform.Translate(Vector3.left * velocidadDeMovimiento * Time.deltaTime);
            }
        }
        else if (transform.localPosition.x - posicionDeseada.x < -tolerancia)
        {
            transform.Translate(Vector3.right * velocidadDeMovimiento * Time.deltaTime);
        }

        if (transform.localPosition.y != posicionDeseada.y)
        {
            if (transform.localPosition.y - posicionDeseada.y > tolerancia)
            {
                transform.Translate(Vector3.down * velocidadDeMovimiento * Time.deltaTime);
            }
        }
        else if (transform.localPosition.y - posicionDeseada.y < -tolerancia)
        {
            transform.Translate(Vector3.up * velocidadDeMovimiento * Time.deltaTime);
        }

        if (transform.localPosition.z - posicionDeseada.z > tolerancia)
        {
            if (transform.localPosition.z > posicionDeseada.z)
            {
                transform.Translate(Vector3.back * velocidadDeMovimiento * Time.deltaTime);
            }
        }
        else if (transform.localPosition.z - posicionDeseada.z < -tolerancia)
        {
            transform.Translate(Vector3.forward * velocidadDeMovimiento * Time.deltaTime);
        }
    }
}
