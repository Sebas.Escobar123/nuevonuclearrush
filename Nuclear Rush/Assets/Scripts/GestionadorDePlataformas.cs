using System.Collections;
using System.Collections.Generic;
//using Unity.Android.Gradle.Manifest;
using UnityEngine;

public class GestionadorDePlataformas : MonoBehaviour
{

    [SerializeField] private GameObject[] plataforma1 = new GameObject[4];
    [SerializeField] private GameObject[] plataforma2 = new GameObject[4];
    [SerializeField] private GameObject[] plataforma3 = new GameObject[4];
    [SerializeField] private GameObject moduloDeTransformacion, plataformaDeVictoria;
    [SerializeField] private int modulosPreTransformacion;

    Transformacion transformacion;
    LevelManager manager;

    int contadorDeModulosPasados;
    int aleatorio, numeroModulos, moduloActual;

    // Start is called before the first frame update
    void Start()
    {
        transformacion = GameObject.Find("LevelManager").GetComponent<Transformacion>();
        //NECESIDADES PARA REINICIAR
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        transformacion.RellenarMatriz();
        manager.victoria = false;
        manager.EstadoVivo(true);
        manager.transformacionActual = 0;
        manager.puntuacionDeUltimaPartida = 0;
        manager.tocandoAcido = false;
        Physics.gravity = new Vector3(0, -30, 0);
        manager.puntuacionSinActualizar = true;
        manager.distanciaDeLaRun = 0;


    }

    public GameObject EscogerPlataforma()
    {
        if(moduloActual == 0)
        {
            numeroModulos = plataforma1.Length;
        }
        else if(moduloActual == 1)
        {
            numeroModulos = plataforma2.Length;
        }
        else if(moduloActual == 2)
        {
            numeroModulos = plataforma3.Length;
        }
        else if(moduloActual == 3)
        {
            numeroModulos = plataforma1.Length;
        }

        GameObject plataformaEscogida = null;

        aleatorio = Random.Range(0, numeroModulos); //Escoger numero aleatorio

        if (contadorDeModulosPasados == modulosPreTransformacion)
        {
            contadorDeModulosPasados = 0;
            moduloActual++;
            if(moduloActual!= 4)
            {

                return moduloDeTransformacion;
            }
        }

        if (moduloActual == 0)
        {
            plataformaEscogida = plataforma1[aleatorio];
        }
        else if (moduloActual == 1)
        {
            plataformaEscogida = plataforma2[aleatorio];
        }
        if (moduloActual == 2)
        {
            plataformaEscogida = plataforma3[aleatorio];
        }
        else if (moduloActual == 3)
        {
            plataformaEscogida = plataforma1[aleatorio];
        }
        else if (moduloActual == 4)
        {
            plataformaEscogida = plataformaDeVictoria;
        }

        contadorDeModulosPasados++;

        return plataformaEscogida;
    }
   
}
