using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPiso : MonoBehaviour
{
    // Start is called before the first frame update

    public float puntoDeDestruccion;
    LevelManager manager;
    private GameObject player;
    //public string[] transformaciones;
    void Start()
    {
        ReferenciarJugador(); // la etiqueta cambia dependiendo del personaje, � como puedo hacer para cambiarla automaticamente?
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ReferenciarJugador();
        if (!manager.victoria)
        {
            transform.position = new Vector3(transform.position.x - manager.velocidad * Time.deltaTime, 0f, 0f);
        }
        

        if(transform.position.x <= puntoDeDestruccion)
        {
            Destroy(gameObject);
        }
    }

    public void ReferenciarJugador()
    {

        
        if(GameObject.Find("Jugador") != null)
        {
            player = GameObject.Find("Jugador");
        }
        else if (GameObject.Find("MonoAra�a") != null)
        {
            player = GameObject.Find("MonoAra�a");
        }
        else if (GameObject.Find("Pajaro") != null)
        {
            player = GameObject.Find("Pajaro");
        }
        else
        {
            Debug.Log("No se encontr� al jugador en la escena");
        }
    }
}
