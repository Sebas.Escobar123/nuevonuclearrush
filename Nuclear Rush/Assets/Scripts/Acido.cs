using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acido : MonoBehaviour
{
    LevelManager manager;
    RazonDeMuerte razon;
    Transformacion transformacion;
    PlayerController controllerHumano;
    Movimiento_TortuAra�a controllerTortu;
    Mov_Pajaro controllerPajaro;
    bool enAcido;
    public float tiempoParaMorir;
    bool cago;

    void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        razon = GameObject.Find("LevelManager").GetComponent<RazonDeMuerte>();
        transformacion = GameObject.Find("LevelManager").GetComponent<Transformacion>();
        Invoke("Referenciar", 0.2f);
    }

    public void Referenciar()
    {
        if (GameObject.Find("Jugador"))
        {
            controllerHumano = GameObject.Find("Jugador").GetComponent<PlayerController>();
        }
        if (GameObject.Find("MonoAra�a"))
        {
            controllerTortu = GameObject.Find("MonoAra�a").GetComponent<Movimiento_TortuAra�a>();
        }
        if (GameObject.Find("Pajaro"))
        {
            controllerPajaro = GameObject.Find("Pajaro").GetComponent<Mov_Pajaro>();
        }
        

       
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Acido")) //Entro en acido, revisar si en medio segundo sigue ah�
        {
            Invoke("SecuenciaDeMuerteEnAcido", tiempoParaMorir);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Acido")) //Revisar si sigue en acido
        {
            manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
            enAcido = true;
            manager.tocandoAcido = true;
            if(manager.ObtenerTrasnformacionActual() == 0)
            {
                controllerHumano.puedeSaltar = false;
                if (!cago)
                {
                    controllerHumano.animator.speed = 0.3f;
                }
            }
            else if (manager.ObtenerTrasnformacionActual()==2)
            {
                manager.EstadoVivo(false); //Si el pajaro toca si caga instant
            }
            
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Acido"))
        {
            enAcido = false;
            manager.tocandoAcido = false;
            controllerHumano.animator.speed = 1f;
            
            
        }
    }

    public void SecuenciaDeMuerteEnAcido()
    {
        if (enAcido) //Despues de medio segundo, sigue en el acido, iniciar mataci�n
        {
            cago = true;
            razon.ActualizarRazonDePerdida("El acido no se toma");
            if (manager.ObtenerTrasnformacionActual() == 0)
            {
                controllerHumano.animator.speed = 1f;
                manager.EstadoVivo(false) ;
                controllerHumano.SecuenciaDeMuerteEnAcido();
            }
            else if (manager.ObtenerTrasnformacionActual() == 1)
            {
                manager.EstadoVivo(false);
            }
            else if (manager.ObtenerTrasnformacionActual()==2)
            {
                manager.EstadoVivo(false);
            }
            
        }
    }
}
