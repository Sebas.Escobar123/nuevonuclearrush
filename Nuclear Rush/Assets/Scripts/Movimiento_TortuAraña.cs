using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento_TortuAraña : MonoBehaviour
{
    LevelManager manager;
    Rigidbody rb;
    public float fuerzaSalto;
    public bool puedeSaltar = true;

    //ANIMACION
    Animator animator;
    public float anguloRotacion = 180f;

    // float gravedadInicial; // Almacena el valor de la gravedad inicial
    public float gravedadW = 90f;
    public float gravedadS = -90f;
    public float gravedadActual;

    public int residuo;

    //Recuperacion

    public float velocidadDeRecuperacion;
    public Vector3 posicionInicial;




    private void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        rb = GetComponent<Rigidbody>();

        animator = GetComponentInChildren<Animator>();
        
        
    }


    void Update()
    {
        if (manager.Vivo)
        {

            RecuperarPosicionInicial();

            if (!manager.tocandoAcido)
            {
                if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
                {
                    if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.A))
                    {
                        manager.CambiarVelocidad(1, 1);
                    }
                    else if (Input.GetKey(KeyCode.D))
                    {
                        manager.CambiarVelocidad(1, 2);
                        animator.SetBool("Corriendo", true);
                        
                    }
                    else if (Input.GetKey(KeyCode.A))
                    {
                        manager.CambiarVelocidad(-1, 2);
                        animator.SetBool("Corriendo", false);
                        
                    }
                }
                else
                {
                    manager.CambiarVelocidad(1, 1);
                }
            }
            else if(manager.tocandoAcido)
            {
                manager.CambiarVelocidad(-1, manager.velocidadInicial - 0.5f);
                puedeSaltar = false; //intento para que cuando toque aciido muerta instantaneamente NO SE SI ESTO AFECTÓ A JERRY
            }
            


            if (Input.GetKeyDown(KeyCode.S) && puedeSaltar)
            {
                if (gravedadW < 0)
                {
                    gravedadActual = (gravedadW); // Invierte el valor de la gravedad -(gravedad)
                    Physics.gravity = new Vector3(0, gravedadActual, 0); // Aplica la nueva gravedad
                    transform.rotation = Quaternion.Euler(0,0,0);
                    if (manager.tocandoAcido)
                    {
                        puedeSaltar = true;
                        
                    }

                }

            }

            if (Input.GetKeyDown(KeyCode.W) && puedeSaltar)
            {
                if (gravedadS > 0)
                {
                    gravedadActual = gravedadS; // Restaura la gravedad inicial
                    Physics.gravity = new Vector3(0, gravedadActual, 0); // Aplica la nueva gravedad
                    transform.rotation = Quaternion.Euler(-180, 0, 0);
                    if (manager.tocandoAcido)
                    {
                        puedeSaltar = true;
                    }
                }

            }
        }

        else
        {
            animator.SetBool("Muerto", true);
        }
        
    }

    private void OnCollisionStay(Collision collision)
    {
        puedeSaltar = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        puedeSaltar = false;
    }

    public void RecuperarPosicionInicial()
    {

        Vector3 posicion = transform.position;
        //Recuperar posición Inicial de a poquito
        if (transform.position.x < posicionInicial.x)
        {
            transform.position = new Vector3(posicion.x + (velocidadDeRecuperacion * Time.deltaTime), posicion.y, posicion.z);
        }
    }




}