using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov_Pajaro : MonoBehaviour
{

    LevelManager manager;
    Rigidbody rb;
    public float fuerzaSalto;
    //public bool puedeSaltar = true;
    // float gravedadInicial; // Almacena el valor de la gravedad inicial
    public float velocidadCaida = 3f;
    public float velocidadSubida = 5f;
    private bool subiendo = false;

    //Animacion
    Animator animator;

    //Recuperacion

    public float velocidadDeRecuperacion;
    public Vector3 posicionInicial;



    private void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        rb = GetComponent<Rigidbody>();

        animator = GetComponentInChildren<Animator>();


    }


    void Update()
    {
        Physics.gravity = new Vector3(0, -30, 0);

        if (manager.Vivo)
        {
            RecuperarPosicionInicial();

            if (!manager.tocandoAcido)
            {
                if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
                {
                    if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.A))
                    {
                        manager.velocidad = manager.velocidadInicial;
                    }
                    else if (Input.GetKey(KeyCode.D))
                    {
                        manager.velocidad = manager.velocidadInicial + 2;
                    }
                    else if (Input.GetKey(KeyCode.A))
                    {
                        manager.velocidad = manager.velocidadInicial - 2;
                    }
                }
                else
                {
                    manager.velocidad = manager.velocidadInicial;
                }


                if (Input.GetKeyDown(KeyCode.W))
                {
                    subiendo = true;
                    rb.velocity = new Vector3(0, velocidadSubida, 0); // Aplica velocidad de subida
                }

                // Si se suelta la tecla W o si el p�jaro no est� subiendo, cae con la gravedad normal
                if (Input.GetKeyUp(KeyCode.W) || !subiendo)
                {
                    rb.velocity += Vector3.down * velocidadCaida * Time.deltaTime; // Aplica gravedad
                }
            }
            else if (manager.tocandoAcido)
            {
                manager.CambiarVelocidad(-1, manager.velocidadInicial - 0.5f);
            }

        }

        else
        {
            animator.SetBool("Muerto", true);
        }
    }

    public void RecuperarPosicionInicial()
    {

        Vector3 posicion = transform.position;
        //Recuperar posici�n Inicial de a poquito
        if (transform.position.x < posicionInicial.x)
        {
            transform.position = new Vector3(posicion.x + (velocidadDeRecuperacion * Time.deltaTime), posicion.y, posicion.z);
        }
    }

}
