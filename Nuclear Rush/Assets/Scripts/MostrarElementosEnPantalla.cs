using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MostrarElementosEnPantalla : MonoBehaviour
{

    public List<GameObject> aDesactivar = new List<GameObject>();
    public TextMeshProUGUI puntuacionDeLaRun, puntuacionTotal,record, distanciaDeLaRun, razonDePerdida;
    public GameObject pantallaDeDerrota;
    LevelManager manager;
    RazonDeMuerte managerRazon;
    public TextMeshProUGUI textoDeVictoria;

    private void Start()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        managerRazon = GameObject.Find("LevelManager").GetComponent<RazonDeMuerte>();
        pantallaDeDerrota.SetActive(false);
    }

    private void Update()
    {
        puntuacionDeLaRun.text = (manager.puntuacion + manager.puntuacionDeUltimaPartida).ToString();

        if (!manager.Vivo)
        {
            //ACTIVA LO DE MUERTE
            pantallaDeDerrota.SetActive(true);
            record.text = "Record: " + manager.record.ToString() + " m";
            puntuacionTotal.text = "" + manager.puntuacion.ToString();
            distanciaDeLaRun.text = "Distancia: " + manager.distanciaDeLaRun.ToString() + " m";
            razonDePerdida.text = managerRazon.razonDePerdida;
            


            //DESACTIVA LO OTRO
            foreach (var objeto in aDesactivar)
            {
                objeto.SetActive(false);
            }

            

        }
        if (manager.victoria)
        {
            textoDeVictoria.text = "GANASTE";
            //DESACTIVA LO OTRO
            foreach (var objeto in aDesactivar)
            {
                objeto.SetActive(false);
            }
            Invoke("Explotar", 2f);
            

        }
    }

    public void Explotar()
    {
        GameObject.Find("Explosion").GetComponent<LuzExplosion>().exploto = true;
        GameObject.Find("Explosion").GetComponent<LuzExplosion>().aS2.PlayOneShot(GameObject.Find("Explosion").GetComponent<LuzExplosion>().explosion);
    }

}
