using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerController : MonoBehaviour
{
    LevelManager manager;

    //DE AUDIO
    public AudioSource audioPasos, audioGeneral;
    public AudioClip audioSalto,audioSalto2, audioDeslizar, audioMuerteEnAcido;

    //DE ANIMACI�N
    public Animator animator;
    

    public bool jugando; //Jugando es para hacer ciertas acciones antes de dejar de estar vivo.
    bool antiDobleSalto;
    public Vector3 posicionInicial;
    Rigidbody rb;
    public float cantidadQueVaria, velocidadDeRecuperacion;
    public float fuerzaSalto;
    public bool puedeSaltar = true;

    bool puedeDeslizarse;
    [SerializeField] float tiempoDeDeslizamiento;
    public GameObject hitBoxParado, hitBoxSentado;

    public int residuo;

    bool puntuacionActualizada;

    //Acido
    bool muriendoEnAcido;

    public void Awake()
    {
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
    }
    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody>();
        antiDobleSalto = true;
        puedeDeslizarse = true;
        hitBoxSentado.SetActive(false);
        jugando = true;
        puntuacionActualizada = false;
    }

    

    void Update()
    {
        
        if (manager.Vivo)
        {
            if (!jugando)
            {
                animator.SetBool("Muerto", true);
            }
            if (jugando)
            {
                if(puedeSaltar && puedeDeslizarse)
                {
                    audioPasos.Play();
                }
                Acelerando();

                Saltar();

                RecuperarPosicionInicial();

                Deslizarse();
            }
            
        }
        else
        {
            audioPasos.Stop();
        }
        if (!jugando && muriendoEnAcido)
        {
            hitBoxParado.SetActive(false);
            hitBoxSentado.SetActive(false);
            rb.useGravity = false;
            manager.velocidad = 0;

            transform.position = new Vector3(transform.position.x, transform.position.y - (0.07f * Time.deltaTime), transform.position.z);
        }




    }
    private void LateUpdate()
    {
        if (!manager.Vivo)
        {
            if (!puntuacionActualizada)
            {
                manager.ActualizarPuntuacion();
                puntuacionActualizada = true;
            }
            animator.Play("Muerte");
            manager.velocidad = 0;



        }
    }


    public void Acelerando()
    {
        if (manager.tocandoAcido)
        {
            manager.CambiarVelocidad(-1, manager.velocidadInicial -0.5f); //reduce velocidad si est� tocando acido
        }
        else
        {
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
            {
                //Debug.Log("Presionando A o D");
                if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.A))
                {
                    animator.SetBool("Lento", false);
                    // Debug.Log("Presionando A y D");
                    manager.CambiarVelocidad(1, 1);
                }
                else if (Input.GetKey(KeyCode.D))
                {
                    animator.SetBool("Lento", false);
                    //Debug.Log("Presionando D");
                    manager.CambiarVelocidad(1, 2);
                }
                else if (Input.GetKey(KeyCode.A))
                {
                    //Debug.Log("Presionando A");
                    animator.SetBool("Lento", true);
                    manager.CambiarVelocidad(-1, 2);
                }
            }
            else
            {
                manager.CambiarVelocidad(1, 1);
            }
        }
    }

    

    public void Saltar()
    {
        //Castear Rasho laser
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, 0.75f, 1) && antiDobleSalto) //Tirar el rayo para abajo
        {
            antiDobleSalto = false;
            Invoke("AntiDobleSalto", 0.1f);
            puedeSaltar = true; //Si le est� pegando al suelo, puede saltar
            animator.SetBool("Saltando", false);
        }

        if (Input.GetKeyDown(KeyCode.W) && puedeSaltar) //Apretar W y estar en el suelo
        {
            rb.AddForce(Vector2.up * fuerzaSalto, ForceMode.Impulse);
            animator.SetBool("Saltando",true);
            if(Random.Range(0, 2) == 0)
            {
                audioGeneral.PlayOneShot(audioSalto);
            }
            else
            {
                audioGeneral.PlayOneShot(audioSalto2);
            }
             audioPasos.Stop();

            puedeSaltar = false;
        }
    }

    public void AntiDobleSalto()
    {
        antiDobleSalto = true;
    }

    public void RecuperarPosicionInicial()
    {

        Vector3 posicion = transform.position;
        //Recuperar posici�n Inicial de a poquito
        if (transform.position.x < posicionInicial.x)
        {
            transform.position = new Vector3(posicion.x + (velocidadDeRecuperacion * Time.deltaTime), posicion.y, posicion.z);
        }
    }

    public void Deslizarse()
    {
        if(puedeSaltar && puedeDeslizarse && Input.GetKeyDown(KeyCode.S)) //Revisar que no est� en el aire ni deslizandose
        {
            CambiarHitBox(); //Cambiar a sentado
            Invoke("CambiarHitBox", tiempoDeDeslizamiento);
            puedeDeslizarse = false;
            animator.Play("Deslizar");
            audioPasos.Stop(); audioGeneral.PlayOneShot(audioDeslizar);

        }
    }

    public void CambiarHitBox()
    {
        if (hitBoxParado.activeSelf)
        {
            hitBoxSentado.SetActive(true); //Se sienta
            hitBoxParado.SetActive(false);
        }
        else
        {
            hitBoxSentado.SetActive(false); //Se para
            hitBoxParado.SetActive(true);
            puedeDeslizarse = true;
        }

    } //Deslizar



    //----ACIDO----
    public void VelocidadEnAcido()
    {
        manager.velocidad = 0.3f;
        animator.SetBool("Lento", true);
    }

    public void VelocidadFueraDeAcido()
    {
        manager.velocidad = manager.velocidadInicial;
    }

    public void SecuenciaDeMuerteEnAcido()
    {
        animator.speed = 1f;
        muriendoEnAcido = true;
        audioGeneral.PlayOneShot(audioMuerteEnAcido);
        jugando = false;
        Invoke("PantallaDeMuerte", 3); 
        //Activar Animaci�n de muerte en acido
        
    }

    public void PantallaDeMuerte() //De momento lo coloco en este script pero ser�a bueno ponerlo en otro lado para no saturar este
    {
        manager.EstadoVivo(false);
        Debug.Log("Muri� :(");
    }


    

}
