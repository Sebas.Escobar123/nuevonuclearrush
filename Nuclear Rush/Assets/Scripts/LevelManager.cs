using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public bool victoria;
    public bool Vivo;
    //Velocidad
    public float velocidad, velocidadInicial;
    public float tiempoDeJuego;
    public int transformacionActual;

    private bool vivo;
    public bool tocandoAcido;

    public int puntuacionDeUltimaPartida;
    public int puntuacion;

    public int distanciaDeLaRun;

    public bool puntuacionSinActualizar;

    public int record;

    void Start()
    {
        velocidadInicial = velocidad;
        vivo = true;
        puntuacionSinActualizar = true;
    }

    private void Update()
    {
        Vivo = vivo;
        if(victoria)
        {
            velocidad = 0;
        }
        if (!vivo) 
        {
            velocidad = 0;

            //Mostrar jugador



            //Actualizar puntuaciones
            if(distanciaDeLaRun > record)
            {
                record = distanciaDeLaRun;
            }

            if (puntuacionSinActualizar)
            {
                ActualizarPuntuacion();
            }

        }
    }

    public int ObtenerTrasnformacionActual()
    {
        return transformacionActual;
    }


    public string NombreTransformacionActual()
    {
        if(transformacionActual == 0 || transformacionActual == 3)
        {
            return "Jugador";
        }
        else if (transformacionActual == 1)
        {
            return "MonoAra�a";
        }
        else if (transformacionActual == 2)
        {
            return "Pajaro";
        }
        Debug.Log("ERROR EN TRANSFORMACION ACTUAL");
        return null;

    }

    public void ActualizarPuntuacion()
    {
        
        if (puntuacionSinActualizar && !vivo)
        {
            puntuacion += puntuacionDeUltimaPartida;
            puntuacionSinActualizar = false;
        }
        


    }

    public void ActualizarPuntuacionActual(int puntosASubir)
    {
        puntuacionDeUltimaPartida += puntosASubir;
        
        
    }


    public void CambiarVelocidad(int direccion, float cantidadQueVaria)
    {
          velocidad = velocidadInicial + (cantidadQueVaria * direccion);
       
    }

    public void EstadoVivo(bool estado)
    {
        if (!victoria)
        {
            vivo = estado;
        }
    }

    
}
