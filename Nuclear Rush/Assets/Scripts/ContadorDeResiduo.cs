using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContadorDeResiduo : MonoBehaviour
{
    public int residuo;
    int largo;

    public GameObject[] elementosCanvasPorResiduo;

    private void Start()
    {
        residuo = 0;
    }


    private void Update()
    {
        int contador = residuo;
        foreach(var elemento in elementosCanvasPorResiduo)
        {
            if(contador > 0)
            {
                elemento.SetActive(true);
                contador--;
            }
            else
            {
                elemento.SetActive(false);
            }
        }
    }


    public void SubirResiduo(int cantidad)
    {
        residuo += cantidad;
        residuo = Mathf.Clamp(residuo,0,5);

    }

    

    



}
