using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzExplosion : MonoBehaviour
{
    int intesidad;
    Light luz;
    public bool exploto;
    public AudioClip alarma, explosion;
    public AudioSource aS1,aS2;
    private void Start()
    {
        luz = GetComponent<Light>();
        
    }

    private void Update()
    {
        if(GameObject.Find("Timer").GetComponent<Timer>().tiempoRestante < 11 && GameObject.Find("Timer").GetComponent<Timer>().tiempoRestante > 0)
        {
            aS1.PlayOneShot(alarma);
        }
        if (GameObject.Find("Timer").GetComponent<Timer>().tiempoRestante < 0)
        {
            aS1.Stop();
            aS2.PlayOneShot(explosion);
        }

        if (exploto)
        {
            if(luz.intensity < 100000)
            luz.intensity += 10;
        }
    }
}
