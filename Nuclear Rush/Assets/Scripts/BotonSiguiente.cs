using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonSiguiente : MonoBehaviour
{
    public GameObject paginaDesactivada;
    public GameObject paginaActiva;
    

    public void OnButtonClick()
    {
        if (paginaDesactivada )
        {
            paginaDesactivada.SetActive(false);
        }

        if (paginaActiva )
        {
           paginaActiva.SetActive(true);
        }
    }
}
