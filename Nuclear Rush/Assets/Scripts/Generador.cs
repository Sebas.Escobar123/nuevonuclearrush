using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generador : MonoBehaviour
{
    GestionadorDePlataformas gestionador;
    GameObject plataforma;
    LevelManager manager;

    public bool instanciado;

    //MediAvance
     bool midiendo;
     float posicionx;
     int distanciaRecorrida;

    public void Start()
    {
        midiendo = false;
        gestionador = GameObject.Find("ControladorPlataformas").GetComponent<GestionadorDePlataformas>();
        manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
    }

    public void FixedUpdate()
    {
        if(transform.position.x < 28 && !instanciado)
        {
            plataforma = gestionador.EscogerPlataforma();
            Instantiate(plataforma, transform.position, Quaternion.identity);
            instanciado = true;

            
            
        }
        if (!midiendo)
        {
            posicionx = transform.position.x;
            midiendo = true;
        }

        MedirDistanciaRecorrida();
    }

    public void MedirDistanciaRecorrida()
    {
        if(posicionx > transform.position.x + 1 && !instanciado)
        {
            
            distanciaRecorrida += 1;
            manager.distanciaDeLaRun +=1;
            manager.ActualizarPuntuacionActual(1);
            midiendo = false;
        }
    }

    



}
