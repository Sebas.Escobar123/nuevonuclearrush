using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    public GameObject ui;
    public float tiempo;
    void Start()
    {
        ui.SetActive(false);
        Invoke("AparecerUI", tiempo);
    }

    void AparecerUI()
    { 
        ui.SetActive(true);
    }
}
