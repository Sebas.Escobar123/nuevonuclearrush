using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RazonDeMuerte : MonoBehaviour
{
    public string razonDePerdida;
    
    public void ActualizarRazonDePerdida(string razon)
    {
        razonDePerdida = razon;
    }
}
