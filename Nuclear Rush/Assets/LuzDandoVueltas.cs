using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzDandoVueltas : MonoBehaviour
{
    public float velocidad;
    public float tiempoADestruir;

    private void Start()
    {
        Destroy(gameObject,tiempoADestruir);
    }
    void Update()
    {
        transform.Rotate(Vector3.up * velocidad * Time.deltaTime);
    }
}
